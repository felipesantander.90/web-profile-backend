FROM python:3.10-buster
ARG PORT_MONGO
ARG SECRET_KEY
ARG LOG_LEVEL
ARG USER_ADMIN_DJANGO
ARG PASSWORD_ADMIN_DJANGO
ARG URL_SERVER_BACKEND
ARG USER_MONGO
ARG PASSWORD_MONGO
ARG HOST_MONGO
ARG PORT_MONGO
ENV SECRET_KEY=${SECRET_KEY}
ENV LOG_LEVEL=${LOG_LEVEL}
ENV USER_ADMIN_DJANGO=${USER_ADMIN_DJANGO}
ENV PASSWORD_ADMIN_DJANGO=${PASSWORD_ADMIN_DJANGO}
ENV URL_SERVER_BACKEND=${URL_SERVER_BACKEND}
ENV USER_MONGO=${USER_MONGO}
ENV PASSWORD_MONGO=${PASSWORD_MONGO}
ENV HOST_MONGO=${HOST_MONGO}
ENV PORT_MONGO=${PORT_MONGO}
WORKDIR /app
COPY . /app/
RUN pip install --upgrade pip -r requirements.txt
