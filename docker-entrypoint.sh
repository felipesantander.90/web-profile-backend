#!/bin/bash
# Collect static files
python src/manage.py makemigrations
python src/manage.py makemigrations core
python src/manage.py migrate
python src/manage.py collectstatic --noinput

cat <<EOF | python src/manage.py shell
from django.contrib.auth import get_user_model

User = get_user_model()  # get the currently active user model,

User.objects.filter(username='admin').exists() or \
    User.objects.create_superuser('admin', '$USER_ADMIN_DJANGO', '$PASSWORD_ADMIN_DJANGO')
EOF

echo "Apply database migrations"
python src/manage.py migrate

# Start server
if [ "$1" = "local" ]; then
    echo "Starting local server"
    python -u src/manage.py runserver 0.0.0.0:8000
fi
if [ "$1" = "server" ]; then
    echo "Starting server server"
    cd src
    gunicorn services.main.wsgi:application -c /app/config/gunicorn/gunicorn.conf.py
fi
