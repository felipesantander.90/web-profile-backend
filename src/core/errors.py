"""errors for API mobile service
"""
from rest_framework.response import Response


def cerberus_error_response(error, status):
    """cerberus_error_response"""
    return Response(error, status=status)


def not_found_error(name_error, description_error):
    """not found error template

    Args:
        name_error (_type_): name error
        description_error (_type_):  description error

    Returns:
        _type_: error template
    """
    return cerberus_error_response(
        {
            "message": name_error,
            "description": description_error,
        },
        404,
    )
