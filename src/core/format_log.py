"""Is a file for formatter log"""
import logging
import json_log_formatter
from django.utils.timezone import now


class CustomisedJSONFormatter(json_log_formatter.JSONFormatter):
    """class for formatter log"""
    def json_record(self, message: str, extra: dict, record: logging.LogRecord):
        """function for formatter json log"""
        extra['name'] = record.name
        extra['filename'] = record.filename
        extra['funcName'] = record.funcName
        extra['msecs'] = record.msecs
        if record.exc_info:
            extra['exc_info'] = self.formatException(record.exc_info)

        return {
            'message': message,
            'timestamp': now(),
            'level': record.levelname,
            'context': extra
        }