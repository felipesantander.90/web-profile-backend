sum_schema = {
    'first_number': { 'required': True, 'type': 'integer' },
    'second_number': { 'required': True, 'type': 'integer' },
}