"""admin model"""
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from django.apps import apps


def auto_register(model):
    """auto register model"""
    search_fields = [f.name for f in model._meta.get_fields() if f.auto_created is False]

    my_admin = type(
        'MyAdmin', (admin.ModelAdmin,),
        {
            'search_fields': search_fields,
            'list_per_page': 50})

    try:
        admin.site.register(model, my_admin)
    except AlreadyRegistered:
        # This model is already registered
        pass

for model in apps.get_app_config('language').get_models():
    auto_register(model)
