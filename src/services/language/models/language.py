import uuid
from djongo import models


class Language(models.Model):
    """TypeContacts model """
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    name = models.TextField()

    def __str__(self):
        return f"{self.name}"
