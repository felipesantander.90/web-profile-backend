from django.urls import path
from .views.language import language_view

# url with language param
urlpatterns = [
    path('', language_view, name='language'),
]
