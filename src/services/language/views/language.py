from django.http.response import JsonResponse
from rest_framework.decorators import api_view
from drf_yasg.utils import swagger_auto_schema
from swagger.services.language.views.language import responses_language
from ..models.language import Language
from ..serializer.language import LanguageSerializer


LENGAUGE = {
    "ES": "spanish",
    "EN": "english",
}

@swagger_auto_schema(method='get', manual_parameters=[], responses=responses_language)
@api_view(['GET'])
def language_view(request):
    """ return all languages for the profile of the user """
    languages = Language.objects.all()
    languages_serializer = LanguageSerializer(languages, many=True)
    return JsonResponse(languages_serializer.data, safe=False)
