from django.apps import AppConfig


class ProfileWebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'services.profile_web'
    verbose_name = 'profile_web'

