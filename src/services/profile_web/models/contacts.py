"""contact model"""
import uuid
from djongo import models
from services.language.models.language import Language
from .type_contacts import TypeContacts


class Contacts(models.Model):
    """Constants model """
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    type_contact = models.ForeignKey(
        TypeContacts,
        on_delete=models.CASCADE,
    )
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE,
    )
    contact = models.TextField()
    objects = models.DjongoManager()

    def __str__(self):
        return f"{self.type_contact.name} - {self.contact} - {self.language.name}"
