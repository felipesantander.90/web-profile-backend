"""profession model"""
import uuid
from djongo import models
from .contacts import Language


class Professions(models.Model):
    """_summary_ = "Professions model"

    Args:
        id (uuid): id of the profession
        name (str): name of the profession
        start_profession (str): start date of the profession
        finish_profession (str): finish date of the profession
        finished (bool): if the profession is finished
        institution (str): institution of the profession
    """
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    name_profession = models.CharField(max_length=300)
    start_profession = models.DateField()
    finish_profession = models.DateField()
    finished = models.BooleanField(default=False)
    institution = models.CharField(max_length=300)
    icon = models.CharField(max_length=100)
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE,
    )
    objects = models.DjongoManager()

    def __str__(self):
        """Return a string representation of the model"""
        return f"{self.name_profession} - {self.language.name}"
