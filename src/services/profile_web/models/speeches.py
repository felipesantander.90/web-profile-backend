"""speches model"""
import uuid

from djongo import models

from services.language.models.language import Language


class Speeches(models.Model):
    """Speeches model """
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    title = models.TextField()
    description = models.TextField()
    image = models.ImageField(upload_to='speeches', blank=True, null=True)
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE,
    )
    objects = models.DjongoManager()

    def __str__(self):
        return f"{self.title} - {self.language.name}"
