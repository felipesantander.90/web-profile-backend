import uuid

from djongo import models


class TypeContacts(models.Model):
    """TypeContacts model """
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    name = models.CharField(max_length=50)
    icon = models.CharField(max_length=100)
    objects = models.DjongoManager()

    def __str__(self):
        return f"{self.name}"
