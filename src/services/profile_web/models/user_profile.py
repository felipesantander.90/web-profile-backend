import uuid

from djongo import models

from .contacts import Contacts
from .professions import Professions
from .speeches import Speeches


class UserProfile(models.Model):
    """This class represents the UserProfile model

    Args:
        name (str): The name of the user
        last_name (str): The last name of the user
        age (int): The age of the user
    """
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    age = models.IntegerField()
    image = models.ImageField(upload_to='profile', blank=True, null=True)
    land_image = models.ImageField(upload_to='profile', blank=True, null=True)
    speeches = models.ManyToManyField(
        Speeches,
        null=True,
        blank=True
    )
    constacts = models.ManyToManyField(
        Contacts,
        null=True,
        blank=True
    )
    professions = models.ManyToManyField(
        Professions,
        null=True,
        blank=True
    )
    objects = models.DjongoManager()

    def __str__(self):
        """Return a string representation of the model"""
        return f"UserProfile({self.name})"
