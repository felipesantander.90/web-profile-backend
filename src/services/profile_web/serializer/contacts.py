"""contact serializer"""
from rest_framework import serializers
from ..models.contacts import Contacts


class ContactsSerializer(serializers.ModelSerializer):
    """contact serializer"""

    class Meta:
        """contact serializer"""
        model = Contacts
        fields = ('__all__')
        depth = 1
