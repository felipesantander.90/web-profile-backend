"""mixin model serializer"""
from rest_framework import serializers
from .contacts import ContactsSerializer
from .user_profile import UserProfileSerializer
from .professions import ProfessionsSerializer
from .speeches import SpeechesIdsSerializer


class userProfileMixin(serializers.Serializer):
    """mixin model serializer"""
    user_profile = UserProfileSerializer()
    user_conctats = ContactsSerializer()
    user_professions = ProfessionsSerializer()
    user_speeches = SpeechesIdsSerializer()
