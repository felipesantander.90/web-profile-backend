"""profession serializer"""
from rest_framework import serializers
from ..models.professions import Professions


class ProfessionsSerializer(serializers.ModelSerializer):
    """Professions serializer"""
    class Meta:
        """meta class"""
        model = Professions
        fields = ('__all__')
