"""speeches serializer"""
from rest_framework import serializers

from ..models.speeches import Speeches


class SpeechesSerializer(serializers.ModelSerializer):
    """speeches serializer"""
    class Meta:
        """meta class"""
        model = Speeches
        fields = ('__all__')

    def get_photo_url(self, speeche):
        request = self.context.get('request')
        photo_url = speeche.photo.url
        return request.build_absolute_uri(photo_url)

class SpeechesIdsSerializer(serializers.ModelSerializer):
    """speeches ids serializer"""
    class Meta:
        """meta class"""
        model = Speeches
        fields = ('id',)
