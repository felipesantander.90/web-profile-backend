"""user profile serializer"""
from rest_framework import serializers

from ..models.user_profile import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    """user profile serializer"""

    class Meta:
        """meta class"""
        model = UserProfile
        fields = ('id',
                  'name',
                  'last_name',
                  'image',
                  'land_image',
                  'age')

