from django.urls import path

from .views.speeches import speeches_view
from .views.user_profile import user_profile_view

# url with language param

urlpatterns = [
    path('', user_profile_view),
    path('speeche/<str:id_speeche>', speeches_view),
]
