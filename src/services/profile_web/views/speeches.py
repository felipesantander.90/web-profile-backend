from cerberus import Validator
from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view

from core.errors import cerberus_error_response, not_found_error
from errors.profile_web.speeches import ERRORS
from services.validator.profile_web.views.speeches import get_speeche_schema
from swagger.services.profile_web.views.speeches import (id_speeche, language,
                                                         response_speeches)

from ..models.speeches import Speeches
from ..serializer.speeches import SpeechesSerializer

LENGAUGE = {
    "es": "spanish",
    "en": "english",
}

def validation_language(language):
    """valitation language

    Args:
        language (_type_): language

    Returns:
        _type_: void
        error: not found language
    """
    if language not in LENGAUGE:
        return not_found_error(
            name_error=ERRORS["speeche"]["name"],
            description_error=ERRORS["speeche"]["lenaguege"]["NotFound"])

def validation_speeche(id_speeche):
    """valitation speeche

    Args:
        id_speeche (_type_): id speeche

    Returns:
        _type_: void
        error: not found speeche
    """
    if not Speeches.objects.filter(id=id_speeche).exists():
        return not_found_error(
            name_error=ERRORS["speeche"]["name"],
            description_error=ERRORS["speeche"]["not_found"])

@swagger_auto_schema(method='get', manual_parameters=[language, id_speeche], responses=response_speeches)
@api_view(['GET'])
def speeches_view(request, language, id_speeche):
    """return one info speeche from user"""
    validator = Validator(get_speeche_schema)
    if validator.validate({
        "language": language,
        "speeche_id": id_speeche
    }) is False:
        return cerberus_error_response(validator.errors, 400)
    lenaguege = LENGAUGE.get(language, None)
    validation_language(lenaguege)
    user_speeche = Speeches.objects.get(id=id_speeche)
    validation_speeche(user_speeche.id)
    user_speeches_serializer = SpeechesSerializer(user_speeche, context={'request': request})
    return JsonResponse(user_speeches_serializer.data, safe=False)
