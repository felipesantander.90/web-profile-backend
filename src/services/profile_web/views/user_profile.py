"""contacts view"""
from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view

from core.errors import not_found_error
from errors.profile_web.user_profile import ERRORS
from swagger.services.profile_web.views.user_profile import (
    language, responses_user_profile)

from ..models.user_profile import UserProfile
from ..serializer.contacts import ContactsSerializer
from ..serializer.mixins import userProfileMixin
from ..serializer.professions import ProfessionsSerializer
from ..serializer.speeches import SpeechesIdsSerializer
from ..serializer.user_profile import UserProfileSerializer

LENGAUGE = {
    "es": "spanish",
    "en": "english",
}

@swagger_auto_schema(method='get', manual_parameters=[language], responses=responses_user_profile)
@api_view(['GET'])
def user_profile_view(request, language):
    """ return all info profile of the user """
    language = LENGAUGE.get(language, None)
    if language is None:
        return not_found_error(
            name_error=ERRORS["user_profile"]["name"],
            description_error=ERRORS["user_profile"]["lenaguege"]["NotFound"]
        )
    info_user = UserProfile.objects.first()
    contacts_user = info_user.constacts.filter(language__name=language)
    professions_user = info_user.professions.filter(language__name=language)
    speeches_user = info_user.speeches.filter(language__name=language)
    conctact_user_serializer = ContactsSerializer(contacts_user, many=True)
    profession_user_serializer = ProfessionsSerializer(professions_user, many=True)
    info_user_serializer = UserProfileSerializer(info_user, context={'request': request})
    speeches_user_serializer = SpeechesIdsSerializer(speeches_user, many=True)
    mix = userProfileMixin(data={
        "user_profile": info_user_serializer.data,
        "user_conctats": conctact_user_serializer.data,
        "user_professions": profession_user_serializer.data,
        "user_speeches": speeches_user_serializer.data
    })
    mix.is_valid()
    return JsonResponse(mix.data, safe=False)
