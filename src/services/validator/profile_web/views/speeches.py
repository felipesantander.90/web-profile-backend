get_speeche_schema = {
    'language': { 'required': True, 'type': 'string' },
    'speeche_id': { 'required': True, 'type': 'string', "maxlength" : 36 },
}
