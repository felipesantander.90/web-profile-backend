from drf_yasg import openapi
from services.language.serializer.language import LanguageSerializer


response_language_200 = openapi.Response(
    'this endpoint return all languages for the profile of the user',
    LanguageSerializer
)

responses_language = {
    200: response_language_200,
}
