from drf_yasg import openapi

from errors.profile_web.speeches import ERRORS
from services.profile_web.serializer.speeches import SpeechesSerializer

schema_language_403 = openapi.Schema(
      '403',
      type=openapi.TYPE_OBJECT,
      description='forbidden',
      properties={
          'message': openapi.Schema(
              type=openapi.TYPE_STRING, description=ERRORS["speeche"]["name"]
            ),
          'description': openapi.Schema(
              type=openapi.TYPE_STRING, description=ERRORS["speeche"]["lenaguege"]["NotFound"]
            ),
        },
      required=['detail']
)

schema_not_fount_speeche = openapi.Schema(
      '404',
      type=openapi.TYPE_OBJECT,
      description='not found',
      properties={
          'message': openapi.Schema(
              type=openapi.TYPE_STRING, description=ERRORS["speeche"]["name"]
            ),
          'description': openapi.Schema(
              type=openapi.TYPE_STRING, description=ERRORS["speeche"]["not_found"]
            ),
        },
        
      required=['detail']
)

language = openapi.Parameter(
    'language',
    openapi.IN_PATH,
    description="is the language of the speeche",
    type=openapi.TYPE_STRING
)

id_speeche = openapi.Parameter(
    'id_speeche',
    openapi.IN_PATH,
    description="is the id of the speeche",
    type=openapi.TYPE_STRING
)

response_speeche_200 = openapi.Response(
    'this endpoint return the speeche',
    SpeechesSerializer
)

response_language_403 = openapi.Response(
    'language not supported',
    schema_language_403
)

response_speeche_404 = openapi.Response(
    'no found speeche',
    schema_not_fount_speeche
)


response_speeches = {
    200: response_speeche_200,
    403: response_language_403,
    404: response_speeche_404,
}
