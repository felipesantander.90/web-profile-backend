from drf_yasg import openapi

from errors.profile_web.user_profile import ERRORS
from services.profile_web.serializer.mixins import userProfileMixin

schema_error_404 = openapi.Schema(
      '404',
      type=openapi.TYPE_OBJECT,
      description='not found',
      properties={
          'message': openapi.Schema(
              type=openapi.TYPE_STRING, description=ERRORS["user_profile"]["name"]
            ),
          'description': openapi.Schema(
              type=openapi.TYPE_STRING, description=ERRORS["user_profile"]["lenaguege"]["NotFound"]
            ),
        },
      required=['detail']
)


language = openapi.Parameter(
    'language',
    openapi.IN_PATH,
    description="is the language of the user",
    type=openapi.TYPE_STRING
)

response_user_profile_404 = openapi.Response(
    'no found language',
    schema_error_404
)

response_user_profile_200 = openapi.Response(
    'this endpoint return the speeches',
    userProfileMixin
)

responses_user_profile = {
    200: response_user_profile_200,
    404: response_user_profile_404,
}
